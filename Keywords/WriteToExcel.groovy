import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.apache.poi.xssf.usermodel.XSSFCell
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFRow

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import java.io.FileOutputStream

import internal.GlobalVariable

public class WriteToExcel {

	//get the workbook
	private XSSFWorkbook getWorkBook(){
		return new XSSFWorkbook()
	}

	//get the sheet
	private XSSFSheet getSheet(XSSFWorkbook book, String sheetName){
		return book.createSheet(sheetName)
	}

	//write the excel to file system
	private void writeToFileSystem(XSSFWorkbook book, String excelPath){
		try{
			FileOutputStream out = new FileOutputStream(excelPath)
			book.write(out)
			out.close()
		}
		catch(Exception e){
			KeywordUtil.markError(e.toString())
		}
	}

	//write attribute data into excel sheet
	@Keyword
	public void writeToExcelSheet(String excelPath, String sheetName, String elementValue, int rowNum, int cellNum){
		XSSFWorkbook book = getWorkBook()
		XSSFSheet sheet = getSheet(book, sheetName)
		XSSFRow row = sheet.createRow(rowNum)
		XSSFCell cell = row.createCell(cellNum)
		cell.setCellValue(elementValue)

		writeToFileSystem(book, excelPath)
	}

}
