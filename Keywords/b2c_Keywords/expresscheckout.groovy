package b2c_Keywords

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testobject.ConditionType
import internal.GlobalVariable

public class expresscheckout {
	// for selecting the payment option
	private String getPaymentXpath(String payment){
		return "//div[@class='PageWrapper__contentWrapper--3FClP']//li['"+payment+"']"
	}

	private TestObject getTestObjectForPayment(String payment){
		TestObject testObject = new TestObject(payment)
		testObject.addProperty("xpath", ConditionType.EQUALS, getPaymentXpath(payment) , true)
		return testObject
	}

	@Keyword
	public void selectPayment(String payment){
		TestObject paymentTestObject = getTestObjectForPayment(payment)
		WebUI.waitForElementVisible(paymentTestObject, 2)
		WebUI.click(paymentTestObject)
	}
}

