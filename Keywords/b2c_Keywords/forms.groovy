package b2c_Keywords

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class forms {

	@Keyword
	public void login(String email, String password){
		WebUI.waitForElementVisible(findTestObject('en_US_OR/Login/Page_Login  Maui Jim/input_Email Address_j_username'), 60)
		WebUI.setText(findTestObject('en_US_OR/Login/Page_Login  Maui Jim/input_Email Address_j_username'), email)
		WebUI.setText(findTestObject('en_US_OR/Login/Page_Login  Maui Jim/input_Password_j_password'), password)
		WebUI.click(findTestObject('en_US_OR/Login/Page_Login  Maui Jim/button_LOGIN'))
	}

	@Keyword
	public void register(String firstName, String lastName, String email, String confirmEmail, String password,
			String confirmPassword){
		WebUI.waitForElementVisible(findTestObject('en_US_OR/Registration/input_First Name_firstName'), 60)
		WebUI.setText(findTestObject('en_US_OR/Registration/input_First Name_firstName'), firstName)
		WebUI.setText(findTestObject('en_US_OR/Registration/input_Last Name_lastName'), lastName)
		WebUI.setText(findTestObject('en_US_OR/Registration/input_Email Address_email'), email)
		WebUI.setText(findTestObject('en_US_OR/Registration/input_Verify Email Address_verifyEmail'),
				confirmEmail)
		WebUI.setText(findTestObject('en_US_OR/Registration/input_Password_pwd'), password)
		WebUI.setText(findTestObject('en_US_OR/Registration/input_Confirm Password_checkPwd'), confirmPassword)
		WebUI.waitForElementVisible(findTestObject('Object Repository/es_MX_OR/robot_checkbox'), 45)
		WebUI.click(findTestObject('Object Repository/es_MX_OR/robot_checkbox'))
		WebUI.click(findTestObject('en_US_OR/Registration/button_Create an account'))
	}

	@Keyword
	public void cardDetails(String cardNumber, String cardHolderName, String expiryDate, String cvv){
		WebUI.waitForElementVisible(findTestObject('en_US_OR/Delego_OR/input_Credit card_mat-input-0'), 120)

		'Enter Credit Card Number'
		WebUI.setText(findTestObject('en_US_OR/Delego_OR/input_Credit card_mat-input-0'), cardNumber)

		'Enter Name of CardHolder'
		WebUI.setText(findTestObject('en_US_OR/Delego_OR/input_Card number_mat-input-1'), cardHolderName)

		'Enter Expiry date of card'
		WebUI.setText(findTestObject('en_US_OR/Delego_OR/input_Cardholder name_mat-input-2'), expiryDate)

		'Enter CVV number'
		WebUI.setText(findTestObject('en_US_OR/Delego_OR/input_Expiry (MMYY)_mat-input-3'), cvv)

		'Click on submit button'
		WebUI.click(findTestObject('en_US_OR/Delego_OR/span_SUBMIT'))
	}

	@Keyword
	public void shippingAddress(String firstName, String lastName, String companyName, String addressLine1, String addressLine2, String city, String zip, String state, String email, String phone){

		WebUI.waitForElementVisible(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_First Name_shippingFirstName'),
				120)

		'Enter First Name'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_First Name_shippingFirstName'), firstName)

		'Enter Last Name'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Last Name_shippingLastName'), lastName)

		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Company Name or co_shippingCompanyName'), companyName)

		'Enter Address1'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Address 1 (Please no PO Boxes)_shippingLine1'), addressLine1)

		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Address 2_shippingLine2'), addressLine2)

		'Enter City'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_City_shippingTownCity'), city)

		'Enter Zipcode'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Zip Code_shippingPostcode'), zip)

		'Select the State'
		WebUI.selectOptionByLabel(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/select_SelectAlabamaAlaskaAmerican SamoaAri_ecaa68'),
				state, true)

		'Verify state is selected'
		WebUI.verifyOptionSelectedByLabel(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/select_SelectAlabamaAlaskaAmerican SamoaAri_ecaa68'),
				state, false, 60)

		'Enter Email'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Email Address_shippingEmail'), email)

		'Enter Phone Number'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_land Islands_shippingPhone1'), phone)
	}

	@Keyword
	public void shippingAddressWithoutState(String firstName, String lastName, String companyName, String addressLine1, String addressLine2, String city, String zip, String email, String phone){

		WebUI.waitForElementVisible(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_First Name_shippingFirstName'),
				45)

		'Enter First Name'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_First Name_shippingFirstName'), firstName)

		'Enter Last Name'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Last Name_shippingLastName'), lastName)

		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Company Name or co_shippingCompanyName'), companyName)

		'Enter Address1'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Address 1 (Please no PO Boxes)_shippingLine1'), addressLine1)

		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Address 2_shippingLine2'), addressLine2)

		'Enter City'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_City_shippingTownCity'), city)

		'Enter Zipcode'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Zip Code_shippingPostcode'), zip)

		'Enter Email'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_Email Address_shippingEmail'), email)

		'Enter Phone Number'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/ShippingAddress/input_land Islands_shippingPhone1'), phone)
	}

	@Keyword
	public void billingAddressItaly(String firstName, String lastName, String companyName, String addressLine1, String addressLine2, String city, String zipcode, String country, String phone){

		'Enter First Name'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/BillingAddress/input_First Name_billingFirstName'), firstName)

		'Enter Last Name'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/BillingAddress/input_Last Name_billingLastName'), lastName)

		'Enter Codice Fiscal'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/BillingAddress/input_Company Name or co_billingCompanyName'), companyName)

		'Enter Address Line 1'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/BillingAddress/input_Address 1_billingLine1'), addressLine1)

		'Enter Address Line 2'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/BillingAddress/input_Address 2_billingLine2'), addressLine2)

		'Enter City'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/BillingAddress/input_City_billingTownCity'), city)

		'Enter Zipcode'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/BillingAddress/input_Zip Code_billingPostcode'), zipcode)

		'Verify state is selected'
		WebUI.verifyOptionSelectedByLabel(findTestObject('en_US_OR/Checkout_Page_OR/BillingAddress/select_Country'),
				country, false, 60)

		'Enter Phone Number'
		WebUI.setText(findTestObject('en_US_OR/Checkout_Page_OR/BillingAddress/input_land Islands_billingPhone1'), null)
	}
}
