package b2c_Keywords

import static org.junit.Assert.*

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class ProductsAndCategory {

	@Keyword
	public void selectCategory(String categoryName){
		String categoryXpath = "//a[contains(text(),'"+categoryName+"')]"
		TestObject test = new TestObject(categoryXpath)
		test.addProperty("xpath", ConditionType.EQUALS, categoryXpath, true)
		WebUI.waitForElementVisible(test, 60)
		WebUI.click(test);
	}

	@Keyword
	public void selectProduct(String productName){
		String productXpath = "//p[contains(text(),'"+productName+"')]"
		TestObject test = new TestObject(productXpath)
		test.addProperty("xpath", ConditionType.EQUALS, productXpath, true)
		WebUI.waitForElementVisible(test, 60)
		WebUI.click(test);
	}
}
