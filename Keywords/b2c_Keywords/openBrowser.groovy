package b2c_Keywords

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class openBrowser {

	//opening the browser
	@Keyword
	public void openBrowserWithURL(String url){
		WebUI.openBrowser(url)
		WebUI.waitForPageLoad(120)
		/*WebUI.maximizeWindow()*/
		WebUI.waitForElementVisible(findTestObject('en_US_OR/Homepage/button_Accept Cookies'), 120, FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.click(findTestObject('en_US_OR/Homepage/button_Accept Cookies'), FailureHandling.CONTINUE_ON_FAILURE)
	}
}
