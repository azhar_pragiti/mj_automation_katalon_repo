package b2c_Keywords

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class DealerLocatorFlow {

	// for selecting the store from stores list
	@Keyword
	public void selectStoreDL(){
		def storeXpath = "//ul[@aria-label='Retailer']/li[1]"
		TestObject object = new TestObject(storeXpath)
		object.addProperty("xpath", ConditionType.EQUALS, storeXpath, true)
		WebUI.waitForElementVisible(object, 60)
		WebUI.click(object)
	}

	// for selecting the route from start location to the store location
	@Keyword
	public void selectRoute(){
		def routeXpath = "//ul[@id='routes-hook']/li[1]"
		TestObject object = new TestObject(routeXpath)
		object.addProperty("xpath", ConditionType.EQUALS, routeXpath, true)
		WebUI.waitForElementVisible(object, 60)
		WebUI.click(object)
	}

	// click on get direction button to get the route on map
	@Keyword
	public void clickOnGetDirectionButton(){
		def buttonXpath = "//button[@aria-label='Get directions']"
		TestObject object2 = new TestObject(buttonXpath)
		object2.addProperty("xpath", ConditionType.EQUALS, buttonXpath, true)
		WebUI.waitForElementVisible(object2, 60)
		WebUI.click(object2)
	}

	// for selecting train transit option to visit the store
	@Keyword
	public void clickOnTrainTransit(){
		def trainXpath = "//li[@id='directions-by-transit']"
		TestObject object = new TestObject(trainXpath)
		object.addProperty("xpath", ConditionType.EQUALS, trainXpath, true)
		WebUI.waitForElementVisible(object, 60)
		WebUI.click(object)
	}

	//clicking on near by button
	@Keyword
	public void clickOnNearByButton(){
		def buttonXpath = "//button[@class='button button--outlined geolocalize']"
		TestObject object = new TestObject(buttonXpath)
		object.addProperty("xpath", ConditionType.EQUALS, buttonXpath, true)
		WebUI.waitForElementVisible(object, 60)
		WebUI.click(object)
	}

	//select sort option as distance
	@Keyword
	public void selectSortOptionAsDistance(){
		def sortButtonXpath = "//*[@id='sort-modal-hook']/div[2]/button"
		TestObject sortOption = new TestObject(sortButtonXpath)
		sortOption.addProperty("xpath", ConditionType.EQUALS, sortButtonXpath, true)
		WebUI.waitForElementVisible(sortOption, 60)
		WebUI.click(sortOption)

		WebUI.delay(2)

		def distanceSortingXpath = "//input[@id='q156']"
		TestObject sortDistance = new TestObject(distanceSortingXpath)
		sortDistance.addProperty("xpath", ConditionType.EQUALS, distanceSortingXpath, true)
		WebUI.waitForElementVisible(sortDistance, 60)
		WebUI.click(sortDistance)
	}
}
